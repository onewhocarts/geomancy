pico-8 cartridge // http://www.pico-8.com
version 29
__lua__
-- geomancy
-- robotinker

-- bug: killing enemy with sweep lets them attack one last time before dying. probably timing issue

function _init()
 is_debug=true
 --should_clear=true
 debug={}
 win=nil
 t=1
 adj={{0,0},{1,0},{1,1},{1,-1},{0,1},{0,-1},{-1,0},{-1,1},{-1,-1}}

 my_cursor={x=1,y=1}
 cursor_valid=false
 move_start={x=1,y=1}
    
 tweens={}
 toasts={}
 flybys={}
 particles={}
 routines={}
 
 my_fire=0
 my_water=0
 my_earth=0
 my_actions=2
 pow=2
 
 🐱_spd=1
 slot_spd=10
 slot_spr={{"★",8},{"♪",9},{"🐱",10}}
 
 control_state="title"
 
 upgrades={}
 cards={}
 card_x=0 --scroll offset
 card_x_target=0
 card_x_gain=0.1
 card_i=1 --selected card
 card_t=0 --tracks start time of active card hover
 grid=
 {
  w=8,
  h=5,
  hilights={},
  get_bounds=function(self,x,y)
    return{(x-1)*16,(y-1)*16,x*16,y*16}
   end,
  draw=function(self)
   	for x=1,self.w do
			  for y=1,self.h do
			   local hilight=false
			   for h in all(self.hilights) do
			    if h[1]==x and h[2]==y then
			     hilight=true
			     break
			    end
			   end

      local bounds=self:get_bounds(x,y)
 		   if hilight then
 		    local c=10
 		    if control_state=="moving" then
 		     c=7
 		    end
	 		   rect(bounds[1]+1,bounds[2]+1,bounds[3]-1,bounds[4]-1,c)
			    --rect(bounds[1],bounds[2],bounds[3],bounds[4],10)
			   end
			   tline(bounds[1],bounds[2],bounds[1],bounds[4],0,0)
      tline(bounds[1],bounds[2],bounds[3],bounds[2],0,0)
			  end
		  end
		  tline(0,16*self.h,16*self.w,16*self.h,0,0)
		  tline(16*self.w,16*self.h,16*self.w,0,0,0)
   end,
  get_center=function(self,x,y)
    x=min(max(1,x),self.w)
    y=min(max(1,y),self.h)
    return
    {
     x=(x-1)*16+8,
     y=(y-1)*16+8
    }
   end,
  put=function(self,e,x,y,x_o,y_o)
   local center=self:get_center(x,y)
   e.x=center.x+x_o
   e.y=center.y+y_o
   e.gx=x
   e.gy=y
  end,
  put_player=function(self,e,x,y)
    self:put(e,x,y,-4,4)
   end,
  put_enemy=function(self,e,x,y)
    self:put(e,x,y,4,4)
   end,
  put_terrain=function(self,e,x,y)
    self:put(e,x,y,0,0)
   end,
 }
 mana_hilight=
 {
  apply=function(self,e)
    blink_all(get_element_color(e.element),phase1of3)
   end
 }
 red_hilight=
 {
  apply=function(self,e)
    blink_all(8,phase1of3)
   end
 }
 mana_preview_hilight=
 {
  apply=function(self,e)
    local c=get_element_color(e.element)
    blink_all(c,solid)
   end
 }
 dmg_hilight=
 {
  apply=function(self,e)
    blink_all(8,phase2of3)
   end
 }
 ondmg_hilight=
 {
  apply=function(self,e)
    blink_all(8,solid)
   end
 }
 attack_hilight=
 {
  apply=function(self,e)
    blink_all(7,solid)
   end
 }
 super_hilight=
 {
  apply=function(self,e)
    blink_all(10)
   end
 }

 base_draw=function(self)
    if self.hilight!=nil then
     self.hilight:apply(self)
    end
    spr(self.sprite,self.x-self.x_o,self.y-self.y_o,self.w,self.h,self.r)
    pal()
   end
 empty_function=function(self)end

 terrain={}
 adj_terrain={}
 marked_terrain={}
 enemies={}
 marked_enemies={} 
 entities={}
 
 title_entities={}
 title_grid={}
 for x=1,8 do
  add(title_grid,{})
  for y=1,8 do
   add(title_grid[x],false)
  end
 end
  
 --cards
 --[[ 
 local look_card=make_card(
  "look",
  0,
  0,
  0,
  0,
  {},
  "",
  0,
  {},
  12)
 --]]
  
 move_card=make_card("move","")
 move_card.pattern=adj
 move_card.spr_spec=14
 move_card.activate=function(self)
   control_state="moving"
   for e in all(adj_terrain) do
    e.hilight=mana_preview_hilight
   end

   move_start={x=player.gx,y=player.gy}
   for p in all(adj) do
	   add(grid.hilights,{player.gx+p[1],player.gy+p[2]})
	  end
  end
 add(cards,move_card)
   
 local attack_card=make_card("leaf","attack")
 attack_card.earth=1
 attack_card.pattern={{1,0},{-1,0},{0,-1},{0,1}}
 attack_card.element="earth"
 add(cards,attack_card)

 attack_card=make_card("fire","attack")
 attack_card.fire=1
 attack_card.pattern={{1,0},{2,0}}
 attack_card.element="fire"
 add(cards,attack_card)

 attack_card=make_card("surf","attack")
 attack_card.water=1
 attack_card.pattern={{0,0},{1,0},{1,1},{1,-1}}
 attack_card.element="water"
 add(cards,attack_card)

 add(cards,make_grow_card("tree",make_tree,6,"earth"))
 add(cards,make_grow_card("pond",make_pond,70,"water"))
 add(cards,make_grow_card("field",make_field,78,"fire"))

 local heal_card=make_card("heal","")
 heal_card.spr_spec=132
 heal_card.earth=1
 heal_card.water=1
 heal_card.fire=1
 heal_card.use=function(self,pow)
   pause_timer=20/🐱_spd
   control_state="pause>card_select"
   player.♥=min(player.♥max,player.♥+pow)
   sfx(10)
   make_particles(20,player.x,player.y,11,0)
   make_particles(20,player.x,player.y,12,0)
  end
 add(cards,heal_card)

 card=make_card("sweep","attack")
 card.pattern={{0,0},{1,0},{-1,0},{0,1},{0,-1}}
 card.actions=1
 card.activate=function(self)
   pow=2
   queue_card_use()
  end
 add(cards,card)

 attack_card=make_card("leaf2","attack")
 attack_card.earth=2
 attack_card.pattern={{1,0},{1,1},{1,-1},{0,1},{0,-1},{-1,0},{-1,1},{-1,-1}}
 attack_card.element="earth"
 add(cards,attack_card)

 attack_card=make_card("vine","attack")
 attack_card.earth=2
 attack_card.water=1
 attack_card.pattern={{1,0},{2,1},{2,-1},{3,1},{3,-1},{4,1},{4,-1}}
 attack_card.element="earth"
 add(cards,attack_card)

 attack_card=make_card("fire2","attack")
 attack_card.fire=2
 attack_card.pattern={{1,0},{1,1},{1,-1},{2,0},{3,0}}
 attack_card.element="fire"
 add(cards,attack_card)

 attack_card=make_card("bolt","attack")
 attack_card.fire=1
 attack_card.earth=1
 attack_card.pattern={{2,0},{2,1},{2,-1},{3,-2},{3,2}}
 attack_card.element="fire"
 add(cards,attack_card) 

 attack_card=make_card("surf2","attack")
 attack_card.water=2
 attack_card.pattern={{0,0},{0,1},{0,-1},{1,0},{1,1},{1,2},{1,-1},{1,-2}}
 attack_card.element="water"
 add(cards,attack_card) 

 attack_card=make_card("hail","attack")
 attack_card.water=1
 attack_card.fire=1
 attack_card.pattern={{2,0},{-2,0},{0,2},{0,-2},{1,2},{2,1},{1,-2},{2,-1},{-1,2},{-2,1},{-2,-1},{-1,-2}}
 attack_card.element="water"
 add(cards,attack_card) 

 attack_card=make_card("burst","attack")
 attack_card.fire=2
 attack_card.earth=1
 attack_card.actions=2
 attack_card.pattern={{0,0},{0,1},{0,-1},{1,0},{-1,0},{1,1},{1,-1},{-1,-1},{-1,1},{-2,0},{-2,1},{-2,-1},{2,0},{2,1},{2,-1},{0,2},{1,2},{-1,2},{0,-2},{-1,-2},{1,-2}}
 attack_card.element="fire"
 add(cards,attack_card)

 attack_card=make_card("spore","attack")
 attack_card.earth=2
 attack_card.water=2
 attack_card.pattern={{1,1},{1,3},{1,-1},{1,-3},{-1,1},{-1,3},{-1,-1},{-1,-3},{3,1},{3,3},{3,-1},{3,-3},{-3,1},{-3,3},{-3,-1},{-3,-3},{2,0},{2,2},{2,-2},{-2,0},{-2,2},{-2,-2},{4,0},{4,2},{4,-2},{-4,0},{-4,2},{-4,-2},{0,2},{0,-2}}
 attack_card.element="earth"
 add(cards,attack_card)

 attack_card=make_card("shard","attack")
 attack_card.water=3
 attack_card.fire=1
 attack_card.pattern={{1,0},{2,0},{3,0},{-1,0},{-2,0},{-3,0},{0,1},{0,2},{0,-1},{0,-2},{-2,-2},{2,2},{2,-2},{-2,2},{-1,-1},{1,1},{1,-1},{-1,1}}
 attack_card.element="water"
 add(cards,attack_card)

 for i=1,#cards do
  cards[i].i=i
 end

 --monster definitions
 fire_imp={"fire","imp"}
 water_imp={"water","imp"}
 earth_imp={"earth","imp"}
 fire_blight={"fire","blight"}
 water_blight={"water","blight"}
 earth_blight={"earth","blight"}
 fire_orc={"fire","orc"}
 water_orc={"water","orc"}
 earth_orc={"earth","orc"}
 fire_thief={"fire","thief"}
 water_thief={"water","thief"}
 earth_thief={"earth","thief"}
 
 --waves
 waves=
 {
  {
   {{fire_imp,1},{water_imp,3},{earth_imp,5}},
   {{water_imp,2},{water_imp,3},{water_imp,4}},
   {{fire_imp,1},{fire_imp,2},{fire_imp,3},{fire_imp,4},{fire_imp,5}},
   {},
   {{water_imp,1},{water_blight,2},{water_blight,3},{water_blight,4},{water_imp,5}},
  },
--
  {
   {{earth_thief,3}},
   {{fire_imp,2},{water_blight,3},{earth_imp,4},{earth_thief,1},{earth_thief,5}},
   {{water_imp,2},{fire_blight,3},{water_imp,4}},
   {},
   {},
   {{fire_blight,1},{fire_blight,2},{fire_orc,3},{fire_blight,4},{fire_blight,5}},
  },
  {
   {{fire_blight,1},{water_imp,3},{earth_blight,5}},
   {{water_blight,1},{water_orc,2},{earth_thief,3},{water_orc,4},{water_blight,5}},
   {{water_imp,2},{water_imp,3},{water_imp,4}},
   {},
   {},
   {{earth_blight,1},{fire_blight,2},{water_orc,3},{fire_blight,4},{water_blight,5}},
  },
  {
   {{earth_thief,2},{earth_thief,4}},
   {{earth_imp,1},{earth_imp,2},{earth_orc,3},{earth_imp,4},{earth_imp,5}},
   {{fire_blight,2},{water_imp,3},{earth_blight,4}},
   {},
   {},
   {{fire_thief,1},{fire_thief,3},{fire_thief,5}},
   {{earth_orc,3},{water_thief,2},{water_thief,4},{earth_imp,1},{earth_imp,5}},
  },
  {
   {{fire_thief,1},{water_blight,3},{earth_thief,5}},
   {{water_blight,2},{earth_blight,3},{water_blight,4}},
   {{water_thief,3},{fire_orc,2},{earth_orc,4}},
   {},
   {},
   {{fire_orc,2},{fire_orc,4},{earth_blight,3}},
  },
  {
   {{water_orc,1},{fire_orc,3},{earth_orc,5}},
   {{water_orc,1},{earth_orc,2},{fire_orc,3},{water_orc,4},{earth_orc,5}},
  }--
 }
 
 spawners={}
 wave_i=1
 turn_i=0
   
 --menu options
 🐱_spd_fast=function()
   🐱_spd=2 
   menuitem(1,"speed up",🐱_spd_fastest)
   return true 
  end
 🐱_spd_fastest=function()
   🐱_spd=5
   menuitem(1,"reset speed",🐱_spd_normal)
   return true
  end
 🐱_spd_normal=function()
   🐱_spd=1
   menuitem(1,"speed up",🐱_spd_fast)
   return true 
  end
  
 🐱_spd_normal()

 --game_over(true)
end

function init()
 sfx(11)
 add_routine(10,play_sound,1)
 entities={}
 terrain={}
 t=0
 
 --entity setup
 make_player(2,3)

 local source=make_tree(3,4)
 make_tree(3,2)
 make_pond(1,2)
 make_field(2,3)
 make_tree(7,2)
 make_pond(7,4)
 for ter in all(terrain) do
  ter.t_0=-500
 end

 make_imp(player.gx+2,player.gy,"earth")

--[[ full terrain test
 for p in all(adj) do
  big_tree=make_tree(grid.w-2+p[1],3+p[2])
  big_tree.t_0=-500
 end
--]]

--[[ orc test
 make_orc(3,4,"fire")
 make_orc(4,4,"water")
 make_orc(4,3,"earth")
--]]

--[[ blight test
 make_blight(3,4,"fire")
 make_blight(4,4,"water")
 make_blight(4,3,"earth")
--]]

--[[ thief test
 make_thief(3,4,"fire")
 make_thief(4,4,"water")
 make_thief(4,3,"earth")
--]]

 --[[ enemy test
 make_imp(7,4,"fire")
 make_imp(8,4,"fire")
 make_imp(8,3,"fire")
 make_imp(3,4,"water")
 make_imp(4,4,"water")
 make_imp(4,3,"earth")
 make_imp(4,5,"earth")
 make_imp(5,4,"earth")
 make_imp(5,5,"fire")
 --]]
 
 update_spawners()
 quick_sort_by_y(entities,1,#entities)
 eval_mana()
 refresh(player)
 make_flyby("defeat all enemy waves!",30,120)
end

function make_grow_card(name,make_fun,spr_spec,element)
 local grow_card=make_card(name,"grow")
 grow_card.element=element
 grow_card.spr_spec=spr_spec
 grow_card.pattern=adj
 grow_card.actions=2
 grow_card.activate=function(self)
   setup_tile_select()
  end
 grow_card.make_fun=make_fun
 grow_card.use=function(self,pow)
   self.make_fun(my_cursor.x,my_cursor.y)
   sort_entities()
   eval_mana()
   sfx(14)
  end
 return grow_card
end

function setup_tile_select()
 control_state="tile_select"
 move_start.x=player.gx
 move_start.y=player.gy
 my_cursor.x=player.gx
 my_cursor.y=player.gy
 update_cursor_valid()
end

function update_cursor_valid()
 cursor_valid=false
 for p in all(grid.hilights) do
  if p[1]==my_cursor.x and p[2]==my_cursor.y then
   cursor_valid=true
   break
  end
 end
end

function sort_hand()
 local castable_att_cards={}
 local non_castable_att_cards={}
 local castable_non_att_cards={}
 local non_castable_non_att_cards={}
 for i=1,#cards do
  local card=cards[i]
  if card!=move_card then
	  if card.card_type=="attack" then
	   if card:can_afford() and card:can_cast() then
	    add(castable_att_cards,card)
	   else
	    add(non_castable_att_cards,card)
	   end
	  else
	   if card:can_afford() and card:can_cast() then
	     add(castable_non_att_cards,card)
	   else
	    add(non_castable_non_att_cards,card)
	   end
	  end
	 end
 end
 
 cards={move_card}
 local i=2
 for list in all({castable_att_cards,castable_non_att_cards,non_castable_non_att_cards,non_castable_att_cards}) do
  for card in all(list) do
   card.i=i
   i+=1
   add(cards,card)
  end
 end
end

function sort_entities()
 quick_sort_by_y(entities,1,#entities)
end

function update_spawners()
 for s in all(spawners) do
  del(spawners,s)
  del(entities,s)
 end
 local wave_length=#waves[wave_i]
 if turn_i>wave_length then
  if #enemies==0 or turn_i-wave_length>=5 then
   if wave_i==#waves then
    game_over(true)
    return
   else
    wave_i+=1
    make_flyby("wave "..wave_i,30,90)
    turn_i=0
    cache_terrain_pos()
    for i=1,5 do
     local x=ceil(rnd()*grid.w)
     local y=ceil(rnd()*grid.h)
     if terrain_pos[x][y]==nil then
      local terrain_roll=rnd()
      if terrain_roll<0.33 then
       make_tree(x,y)
      elseif terrain_roll<0.66 then
       make_pond(x,y)
      else
       make_field(x,y)
      end
     end
    end
   end
  end
 end
 
 local my_wave=waves[wave_i]
 if win==nil then
  if turn_i>0 and turn_i<=#my_wave then
   for s in all(my_wave[turn_i]) do
    local monster_data=s[1]
    if monster_data[2]=="imp" then
     make_imp(grid.w,s[2],monster_data[1])
    elseif monster_data[2]=="orc" then
     make_orc(grid.w,s[2],monster_data[1])
    elseif monster_data[2]=="blight" then
     make_blight(grid.w,s[2],monster_data[1])
    elseif monster_data[2]=="thief" then
     make_thief(grid.w,s[2],monster_data[1])
    end
   end
   sort_entities()
  end
  turn_i+=1
  if turn_i>0 and turn_i<=#my_wave then
   for s in all(my_wave[turn_i]) do
    local spawner=make_entity(44,1,2)
    grid:put(spawner,grid.w,s[2],4,5)
    spawner.element=s[1][1]
    spawner.hilight=mana_hilight
    add(spawners,spawner)
   end
  end
 end 
end

function eval_mana()
 my_fire=0
 my_water=0
 my_earth=0
 adj_terrain=get_adj_terrain()
 for e in all(adj_terrain) do
  if e.element=="fire" then my_fire+=1
  elseif e.element=="water" then my_water+=1
  elseif e.element=="earth" then my_earth+=1
  end
 end
 sort_hand()
end

function cache_terrain_pos()
 terrain_pos={}
 for x=1,grid.w do
  local col=add(terrain_pos,{})
  for y=1,grid.h do
   add(col,nil)
  end
 end
 for e in all(terrain) do
  terrain_pos[e.gx][e.gy]=e
 end
end

function get_element_color(element)
 local c=0
 if element=="earth" then
  c=11
 elseif element=="water" then
  c=12
 elseif element=="fire" then
  c=9
 end
 return c
end

function get_adj_terrain()
 local out={}
 for p in all(adj) do
  local x=player.gx+p[1]
  local y=player.gy+p[2]
  for e in all(terrain) do
   if e.gx==x and e.gy==y then
    add(out,e)
   end
  end
 end
 return out
end

function blink_all(c,f)
 if (f==nil and t%30>10)
   or (f!=nil and f()) then
  for i=0,16 do
   pal(i,c)
  end
 end
end

function phase1of3()
 return t%30<10
end

function phase2of3()
 return t%30>=10 and t%30<20
end

function phase3of3()
 return t%30>20
end

function solid()
 return true
end

function hurt(e)
 local card=cards[card_i]
 local my_pow=pow
 if e.element=="fire" and card.element=="water"
  or e.element=="water" and card.element=="earth"
  or e.element=="earth" and card.element=="fire"
  then
   my_pow=flr(my_pow*1.5)
 elseif e.element=="earth" and card.element=="water"
  or e.element=="fire" and card.element=="earth"
  or e.element=="water" and card.element=="fire"
  then
   my_pow=ceil(my_pow/3)
 end
 my_pow=min(6,my_pow)
 e.♥-=my_pow
 e.hilight=ondmg_hilight
 
 --particles based on lvl
 local c=4 --4,9,10,6,7,14
 if my_pow==2 then c=9
 elseif my_pow==3 then c=10
 elseif my_pow==4 then c=6
 elseif my_pow==5 then c=7
 elseif my_pow>5 then c=14
 end
 make_particles(4*my_pow,e.x,e.y,c,0.4)
 
 sfx(my_pow+3)
end

function post_hurt(e)
 e.hilight=nil
 if e.♥<=0 then
  del(entities,e)
  del(enemies,e)
 end
end

function make_particles(n,x,y,c,v_0)
 for i=1,n do
  local my_x=x+rnd()*3-1.5
  local my_y=y+rnd()*2-1
	 local out=
	 {
	  x=my_x,
	  y=my_y,
	  vx=(my_x-x)*0.2+v_0,
	  vy=my_y-y-1.5,
	  ddy=0.1,
	  c=c,
	  life=20,
	  update=function(self)
	    self.life-=1
	    if self.life<=0 then
	     del(particles,self)
	    else
	     self.vy+=self.ddy
		    self.x+=self.vx
		    self.y+=self.vy
	    end
	   end,
	  draw=function(self)
	    line(self.x,self.y,self.x,self.y,self.c)
	   end
	 }
	 add(particles,out)
	end
end

function make_flyby(m,y,life)
 local out=
 {
  message=m,
  x=128,
  x_target=64-2*#m,
  y=y,
  life=life,
  t=0
 }
 add(flybys,out)
 return out
end

function make_toast(x,y,sprite,w,h,bg,life)
 local out=
 {
  x=x,
  y=y,
  sprite=sprite,
  w=w,
  h=h,
  bg=bg,
  life=life
 }
 add(toasts,out)
 return out
end

function make_entity(sprite,w,h)
 local out=
 {
  x_o=4*w,
  y_o=8*h-3,
  sprite=sprite,
  w=w,
  h=h,
  r=false,
  update=empty_function,
  draw=base_draw,
 }
 add(entities,out)
 return out
end

function make_tree(x,y)
 return make_source(x,y,"earth",{36,38,16,6})
end

function make_pond(x,y)
 return make_source(x,y,"water",{64,66,68,70})
end

function make_field(x,y)
 return make_source(x,y,"fire",{72,74,76,78})
end

function make_source(x,y,element,phases)
 local out=make_entity(phases[1],2,2)
 out.element=element
 out.t_0=t
 out.update=function(self)
   if t-self.t_0>90 then self.sprite=phases[4]
   elseif t-self.t_0>60 then self.sprite=phases[3]
   elseif t-self.t_0>30 then self.sprite=phases[2]
   end
  end
 out.shuffle_pool=function(self)
   local new_pool={}
   while #self.pool>0 do
    local choice=ceil(rnd()*#self.pool)
    add(new_pool,self.pool[choice])
    del(self.pool,self.pool[choice])
   end
   self.pool=new_pool
  end
 out.pool={"★","♪","♪","♪","♪"}
 out:shuffle_pool()
 grid:put_terrain(out,x,y)
 add(terrain,out)
 return out
end

function make_orc(x,y,element)
 local out=make_entity(45,1,2)
 out.♥=10
 out.♥max=10
 out.spd=1
 out.str=2
 out.element=element
 out.priority="player"
 out.anim=make_anim(
  {0,30,60},
  {45,46,45},
  {false,false,false})
 out.draw=function(self)
   pal(7,0)
   if self.element=="water" then
    pal(5,12)
    pal(6,1)
   elseif self.element=="earth" then
    pal(5,11)
    pal(6,3)
   elseif self.element=="fire" then
    pal(5,9)
    pal(6,8)
   end
   base_draw(self)
   draw_health(self)
  end
 out.update=function(self)
   self.anim:animate(self)
  end
 grid:put_enemy(out,x,y)
 add(enemies,out)
 return out
end

function make_imp(x,y,element)
 local out=make_entity(1,1,1)
 out.♥=3
 out.♥max=3
 out.spd=2
 out.str=1
 out.element=element
 out.priority="terrain"
 out.anim=make_anim(
  {0,30,60},
  {1,2,1},
  {false,false,false})
 out.draw=function(self)
   pal(7,0)
   if self.element=="water" then
    pal(8,1)
    pal(9,12)
    pal(10,12)
   elseif self.element=="earth" then
    pal(8,3)
    pal(9,11)
   end
   base_draw(self)
   draw_health(self)
  end
 out.update=function(self)
   self.anim:animate(self)
  end
 grid:put_enemy(out,x,y)
 add(enemies,out)
 return out
end

function make_blight(x,y,element)
 local out=make_entity(1,1,1)
 out.♥=5
 out.♥max=5
 out.spd=3
 out.str=2
 out.element=element
 out.priority="terrain"
 out.anim=make_anim(
  {0,30,60},
  {47,63,47},
  {false,false,false})
 out.draw=function(self)
   pal(1,0)
   if self.element=="water" then
    pal(8,1)
    pal(9,12)
   elseif self.element=="earth" then
    pal(8,3)
    pal(9,11)
   end
   base_draw(self)
   draw_health(self)
  end
 out.update=function(self)
   self.anim:animate(self)
  end
 grid:put_enemy(out,x,y)
 add(enemies,out)
 return out
end

function make_thief(x,y,element)
 local out=make_entity(1,1,2)
 out.♥=5
 out.♥max=5
 out.spd=3
 out.str=1
 out.element=element
 out.priority="player"
 out.anim=make_anim(
  {0,30,60},
  {34,35,34},
  {false,false,false})
 out.draw=function(self)
   pal(7,0)
   if self.element=="water" then
    pal(6,1)
    pal(5,12)
   elseif self.element=="earth" then
    pal(6,3)
    pal(5,11)
   elseif self.element=="fire" then
    pal(6,8)
    pal(5,9)
   end
   base_draw(self)
   draw_health(self)
  end
 out.update=function(self)
   self.anim:animate(self)
  end
 grid:put_enemy(out,x,y)
 add(enemies,out)
 return out
end

function make_player(x,y)
 local out=make_entity(26,1,2)
 out.♥=7
 out.♥max=7
 out.anim=make_anim(
  {0,24,28,44,48,56},
  {26,25,24,26,27,26},
  {false,false,false,false,false,false,false})
 out.update=function(self)
   if self.♥>0 then
    self.anim:animate(self)
   end
  end
 out.draw=function(self)
   base_draw(self)
   draw_health(self)
  end
 grid:put_player(out,x,y)
 player=out
 return out
end

function draw_health(e)
 local left=e.x-3
 local top=e.y-6-8*(e.h-1)
 if e.gy==1 then
  top+=8*e.h
 end
 line(left,top,left+e.♥max,top,0)
 if e.♥>0 then
  local c=11
  if e.♥<=e.♥max/3 then
   c=8
  end
  line(left,top,left+e.♥,top,c)
 end
end

function make_anim(ts,sprs,rs)
 return
 {
  ts=ts,
  sprs=sprs,
  rs=rs,
  animate=function(self,entity)
    local last_t=self.ts[#self.ts]
    local my_t=t%last_t
    local i=#self.ts
    while i>0 do
     if my_t>self.ts[i] then
       entity.sprite=self.sprs[i]
       entity.r=self.rs[i]
      break
     end
     i-=1
    end
   end
 }
end

function make_card(name,c_t)
 local card=
 {
  name=name,
  i=0,
  x=0,
  actions=1,
  earth=0,
  water=0,
  fire=0,
  pattern={},
  card_type=c_t,
  element="",
  spr_spec=nil,
  activate=function(self)
   end,
  use=function(self,pow)
   end,
  can_afford=function(self)  
    return my_actions>=self.actions and my_earth>=self.earth and my_water>=self.water and my_fire>=self.fire
   end,
  can_cast=function(self)  
    if self.card_type!="attack" then
     return true
    else
     for 🐱 in all(enemies) do
      for p in all(self.pattern) do
       if 🐱.gx==player.gx+p[1] and 🐱.gy==player.gy+p[2] then
        return true
       end
      end
     end
    end
    return false
   end,
  update=function(self)
    self.x+=((self.i-1)*24-self.x)*0.2
   end,
  draw=function(self,x,y,active)
    local c=13
    local b=5
    local castable=self:can_afford() and self:can_cast()
    if castable then
     c=6
     b=13
	    if active then
	     c=7
	     b=1
	    end
    end
    x+=self.x
    local right=x+24
    local bottom=y+32
    rectfill(x,y,right,bottom,c)
    rect(x,y,right,bottom,b)
    
    --mana cost
    local mana_x=2
	   for m in all({{self.earth,11,my_earth,3},{self.water,12,my_water,1},{self.fire,9,my_fire,4}}) do
	    if m[1]>0 then
	     for i=1,m[1] do
	      local my_x=x+mana_x
	      local my_y=y+30-4*(i-1)
	      if m[3]<i then
 	      rect(my_x,my_y,my_x+2,my_y-2,m[4])
							else
 	      rectfill(my_x,my_y,my_x+2,my_y-2,m[2])
	      end
	     end
	     mana_x+=4
	    end
 	  end
 	  
    --pattern
    if self.spr_spec!=nil then
     spr(self.spr_spec,x+4,y+10,2,2)
    elseif #self.pattern>0 then
     local c_p=4
     c=2
     if affordable and active then 
      c_p=9
      c=8
     end
     local x_p=x+12
     local y_p=y+16
     rect(x_p,y_p,x_p,y_p,c_p)
     for p in all(self.pattern) do
      if p[1]!=0 or p[2]!=0 then
       local my_x=x_p+2*p[1]
       local my_y=y_p+2*p[2]
       rect(my_x,my_y,my_x,my_y,c)
      end
     end
    end

    local c=5
    if self.element=="fire" then
     if castable then c=8 else c=4 end
    elseif self.element=="earth" then
     c=3
    elseif self.element=="water" then
     if castable then c=12 else c=1 end
    end
    print(self.name,x+3,y+3,c)
    
    --actions
    for i=1,self.actions do
     local c=5
     if my_actions>=i then
      c=1
     end
     print("⧗",x+17-6*(i-1),y+26,c)
    end
   end
 }
 return card
end

function _update()
 t+=1

 if control_state=="title" then
	 if t%30==0 or t%30==10 then
	  local roll=rnd()
	  local x=ceil(rnd()*8)
	  local y=ceil(rnd()*8)
	  if not title_grid[x][y] then
	   local new_thing=nil
	   if roll>0.66 then
	    new_thing=make_tree(x,y)
	   elseif roll>0.33 then
	    new_thing=make_pond(x,y)
	   else
	    new_thing=make_field(x,y)
	   end
	   new_thing.y=16*y-3
	   add(title_entities,new_thing)
    title_grid[x][y]=true
   end
  end
  
  if btnp(🅾️) or btnp(❎) then
   init()
  end
  
	 return 
 end

 for toast in all(toasts) do
  toast.life-=1
  if toast.life<=0 then
   del(toasts,toast)
  end
 end
 
 foreach(particles,do_update)
 foreach(entities,do_update)
 
 for tween in all(tweens) do
  local diff_x=tween.dest.x-tween.entity.x
  local diff_y=tween.dest.y-tween.entity.y
  tween.entity.x+=diff_x*tween.gain*🐱_spd
  tween.entity.y+=diff_y*tween.gain*🐱_spd
  if abs(diff_x)<1 and abs(diff_y)<1 then
   tween.entity.tweening=false
   del(tweens,tween)
  end
 end
 if #tweens>0 then
  sort_entities()
 end
 
 if #routines>0 then
  local r=routines[1]
  r[1]-=1
  if r[1]<=0 then
   if r[2]!=nil then
    r[2](r[3])
   end
   del(routines,r)
  end
 end
 
 foreach(cards,do_update)
 card_x+=(card_x_target-card_x)*card_x_gain
 
 if control_state=="card_select" then
  if my_actions<=0 then
   control_state="enemy_turn"
   update_spawners()
   for e in all(enemies) do
    add_routine(10,take_turn,e)
   end
   add_routine(20,refresh,player)
  end
  if btnp(⬅️) then
   local prev_card_i=card_i
   card_i=max(1,card_i-1)
   if card_i!=prev_card_i then
    start_homing_hand()
    sfx(12)
   end
  elseif btnp(➡️) then
   local prev_card_i=card_i
   card_i=min(#cards,card_i+1)
   if card_i!=prev_card_i then
    start_homing_hand()
    sfx(12)
   end
  elseif btnp(❎) or btnp(🅾️) then
   local card=cards[card_i]
   if card:can_afford()
    and (#card.pattern==0
     or #grid.hilights>0)
    and (card.card_type!="attack"
     or #marked_enemies>0)
     then
    my_actions-=card.actions
    sfx(13)
    if #marked_terrain>0 then
     control_state="rolling"
     pow=2
     clear_preview_vfx()
     marked_terrain[1].hilight=red_hilight
     sfx(0,3)
    else
     card:activate()
    end
   end
  end
  
  if btnp(⬅️) or btnp(➡️) then
   show_preview_vfx()
  end
 elseif control_state=="rolling" then
  local current=marked_terrain[1]
  result=current.pool[flr(t/30*slot_spd)%#current.pool+1]
  if btnp(🅾️) or btnp(❎) then
   pause_timer=40/🐱_spd
   if #marked_terrain>1 then
    control_state="pause>rolling"
   else
    queue_card_use()
   end
   sfx(-1,3)
   
   local left=min(current.x+6,115)
   local top=max(current.y-12,0)
   make_toast(left,top,get_icon(result),1,1,7,25)
   current.hilight=nil
   if result=="🐱" then
    --destroy terrain obj
    sfx(3,2)
    destroy_terrain(current)
    --supercharge
    pow+=2
    player.hilight=super_hilight
   elseif result=="★" then
   	--supercharge
   	sfx(2,2)
   	pow+=1
   elseif result=="♪" then
    --replace ♪ with 🐱 in pool
    del(current.pool,"♪")
    add(current.pool,"🐱")
    current:shuffle_pool()
    sfx(1,2)
   end
   del(marked_terrain,current)
  end
 elseif control_state=="tile_select" then
  if btnp(⬆️) and my_cursor.y>1 and my_cursor.y>move_start.y-1 then
   my_cursor.y-=1
   update_cursor_valid()
  elseif btnp(⬇️) and my_cursor.y<grid.h and my_cursor.y<move_start.y+1 then
   my_cursor.y+=1
   update_cursor_valid()
  elseif btnp(➡️) and my_cursor.x<grid.w and my_cursor.x<move_start.x+1 then
   my_cursor.x+=1
   update_cursor_valid()
  elseif btnp(⬅️) and my_cursor.x>1 and my_cursor.x>move_start.x-1 then
   my_cursor.x-=1
   update_cursor_valid()
  elseif btnp(🅾️) or btnp(❎) then
   if cursor_valid then
	   control_state="card_select"
	   cards[card_i]:use()
	   show_preview_vfx()
   end
  end
 elseif control_state=="moving" then
  local moved=false
  if btnp(⬆️) and player.gy>1 and player.gy>move_start.y-1 then
   tween_to(player,player.gx,player.gy-1,-4,4,0.2)
   moved=true
  elseif btnp(⬇️) and player.gy<grid.h and player.gy<move_start.y+1 then
   tween_to(player,player.gx,player.gy+1,-4,4,0.2)
   moved=true
  elseif btnp(➡️) and player.gx<grid.w and player.gx<move_start.x+1 then
   tween_to(player,player.gx+1,player.gy,-4,4,0.2)
   moved=true
  elseif btnp(⬅️) and player.gx>1 and player.gx>move_start.x-1 then
   tween_to(player,player.gx-1,player.gy,-4,4,0.2)
   moved=true
  elseif btnp(❎) or btnp(🅾️) then
   control_state="card_select"
   if player.gx==move_start.x and player.gy==move_start.y then
    my_actions+=cards[card_i].actions
   end
   show_preview_vfx()
   sfx(12)
  end
  if moved then
   for e in all(terrain) do
    e.hilight=nil
   end

   eval_mana()
   for e in all(adj_terrain) do
    e.hilight=mana_preview_hilight
   end
  end
 elseif control_state=="game_over" then
  if win==true and (t%60==50 or t%60==35 or t%60==15) then
   sfx(4)
   local c=8
   local roll=rnd()
   if roll>0.75 then c=10
   elseif roll>0.5 then c=11
   elseif roll>0.25 then c=12
   end
   make_particles(20,5+rnd()*118,5+rnd()*(16*grid.h-5),c,0)
  end
 elseif sub(control_state,1,5)=="pause" then
  pause_timer-=1
  if pause_timer<=0 then
   control_state=sub(control_state,7)
   if control_state=="card_select" then
    eval_mana()
    show_preview_vfx()
   elseif control_state=="rolling" then
    marked_terrain[1].hilight=red_hilight
    sfx(0,3)
   end
  end
 end
end

function queue_card_use()
 if pause_timer==nil then
  pause_timer=0
 end
 pause_timer+=20/🐱_spd
 control_state="pause>card_select"
 pow=min(6,pow)
 add_routine(20,use_current_card,nil)
end

function use_current_card()
 if cards[card_i].card_type=="attack" then
  local t_seg=pause_timer/(#marked_enemies+1)/🐱_spd
  for i=1,#marked_enemies do
   add_routine(t_seg/2,hurt,marked_enemies[i])
   add_routine(t_seg/2,post_hurt,marked_enemies[i])
  end
 else
  cards[card_i]:use(pow)
 end
 clear_hilight(player)
end

function start_homing_hand()
 if card_i>=3 and #cards>5 then
  card_x_target=(card_i-3)*(-24)
 end
end

function add_routine(timer,f,e,i)
 if i==nil then i=#routines+1 end
 add(routines,{timer/🐱_spd,f,e},i)
end

function destroy_terrain(e)
 del(terrain,e)
 del(entities,e)
 e.is_dead=true
 if not control_state=="rolling" then
  eval_mana()
 end
 --particle effects
 make_particles(30,e.x,e.y,get_element_color(e.element),0)
 sfx(8)
end

function take_turn(e)
 e.moves_left=e.spd
 if e.priority=="terrain" then
  cache_terrain_pos()
  if terrain_pos[e.gx][e.gy]!=nil then
   --hurt terrain
   attack_terrain(e)
  else
   local target=nil
   for d in all(adj) do
    local x=e.gx+d[1]
    local y=e.gy+d[2]
    if x>=1 and x<=grid.w and y>=1 and y<=grid.h then
     local occupied=is_occupied(x,y)
     if not occupied and terrain_pos[x][y]!=nil then
      target=terrain_pos[x][y]
      break
     end
    end
   end
   
   if target!=nil then
    --move to target
    tween_to(e,target.gx,target.gy,4,4)
		  --schedule attack
    add_routine(30,attack_terrain,e,1)
   else
    chase_player(e)
   end
  end
 else
  chase_player(e)
 end
end

function do_update(e)
 e:update()
end

function do_draw(e)
 e:draw()
end

function is_occupied(x,y)
 local occupied=false
 for 🐱 in all(enemies) do
  if 🐱.gx==x and 🐱.gy==y then
   occupied=true
   break
  end
 end
 return occupied
end

function chase_player(e)
 if player.gx==e.gx and player.gy==e.gy then
  maybe_attack_player(e)
 else
  local can_move=move_toward_player(e)
	 if can_move then
		 if e.moves_left>0 then
		  add_routine(30,chase_player,e,1)
		 else
 		 --schedule attack
	 	 add_routine(30,maybe_attack_player,e,1)
		 end
 	end
 end 
end

function move_toward_player(e)
 e.moves_left-=1
 local x_diff=player.gx-e.gx
 if x_diff!=0 then
  x_diff/=abs(x_diff)
 end
 local y_diff=player.gy-e.gy
 if y_diff!=0 then
  y_diff/=abs(y_diff)
 end
 
 local new_x=e.gx+x_diff
 local new_y=e.gy+y_diff
 local occupied=is_occupied(new_x,new_y)
 if occupied then
  occupied=is_occupied(new_x,e.gy)
  if not occupied then
   new_y=e.gy
  end
 end
 if occupied then
  occupied=is_occupied(e.gx,new_y)
  if not occupied then
   new_x=e.gx
  end
 end
 if occupied and y_diff==0 then
  occupied=is_occupied(new_x,e.gy+1)
  if not occupied then
   new_y=e.gy+1
  else
	  occupied=is_occupied(new_x,e.gy-1)
	  if not occupied then
	   new_y=e.gy-1
	  end
  end
 end
 if occupied and x_diff==0 then
  occupied=is_occupied(e.gx+1,new_y)
  if not occupied then
   new_x=e.gx+1
  else
	  occupied=is_occupied(e.gx-1,new_y)
	  if not occupied then
	   new_x=e.gx-1
	  end
  end
 end
 
 if not occupied then
	 --move to target
	 tween_to(e,new_x,new_y,4,4)
	end
	return not occupied
end

function tween_to(e,x,y,x_o,y_o,gain)
 e.gx=x
 e.gy=y
 local dest=grid:get_center(x,y)
 dest.x+=x_o
 dest.y+=y_o
 for tween in all(tweens) do
  if tween.entity==e then
   del(tweens,tween)
  end
 end
 if gain==nil then gain=0.1 end
 add(tweens,{entity=e,dest=dest,gain=gain})
 e.tweening=true
end

function refresh(p)
 if win==nil then 
	 my_actions=3
	 eval_mana()
	 show_preview_vfx()
	 control_state="card_select"
 end
end

function clear_hilight(e)
 e.hilight=nil
end

function attack_terrain(e)
 local target=terrain_pos[e.gx][e.gy]
 schedule_attack(e,target)
end

function maybe_attack_player(e)
 local target=nil
 if e.priority=="terrain" then
  target=maybe_attack_terrain(e)
	 if target!=nil then
	  return
	 end
	end
 for p in all(adj) do
	 if e.gx+p[1]==player.gx and e.gy+p[2]==player.gy then
	  target=player
	  schedule_attack(e,target)
	 end
	end
	if target==nil then
	 maybe_attack_terrain(e)
	end
end

function maybe_attack_terrain(e)
 for p in all(adj) do
	 local x=e.gx+p[1]
	 local y=e.gy+p[2]
	 if x>=1 and x<=grid.w and y>=1 and y<=grid.h
	  and terrain_pos[x][y]!=nil then
	  target=terrain_pos[x][y]
	  schedule_attack(e,target)
	  return target
	 end
	end
end

function schedule_attack(e,target)
 e.hilight=attack_hilight
 show_health(target)
 dmg=e.str
 local commands=
 {
  {10,clear_hilight,e},
  {0,play_sound,7},
  {0,show_dmg_hilight,target},
  {10,clear_hilight,target},
  {0,resolve_dmg,target},
  {0,show_health,target},
  {20,nil,nil},  
 }
 --8132
 for c_i=1,#commands do
  local c=commands[c_i]
  add_routine(c[1],c[2],c[3],c_i)
 end
end

function resolve_dmg(e)
 make_particles(10,e.x,e.y-4,8,0)
 if e==player then
  e.♥-=dmg
  if e.♥<=0 then
   game_over(false)
  end
 else
  local found=false
  for i=1,dmg do
	  for n in all(e.pool) do
	   if n=="♪" then
	    del(e.pool,n)
	    add(e.pool,"🐱")
	    found=true
		   break
	   end
	  end
	  if not found then
	   destroy_terrain(e)
	   break
	  end
	 end
 end
end

function show_health(e)
 if e!=player then
  local left=min(max(e.x-20,0),83)
  local top=max(0,e.y-20)
  local health=1
	 for n in all(e.pool) do
   if n=="♪" then
    health+=1
   end
  end
  for i=1,#e.pool do
   local sprite=9
   if i==1 then
    sprite=8
    if e.is_dead then
     sprite=10
    end
   elseif i>health then
    sprite=10
   end
   --x,y,sprite,w,h,bg,life
   make_toast(left+9*(i-1),top,sprite,1,1,6,30)
  end
 end
end

function game_over(w)
 win=w
 clear_preview_vfx()
 control_state="game_over"
 if w==false then
  player.♥=0
  player.x_o+=4
  player.y_o-=12
  player.anim=nil
  player.sprite=56
  player.w=2
  player.h=1
 else
  cache_terrain_pos()
  for x=1,grid.w do
   for y=1,grid.h do
    if terrain_pos[x][y]==nil then
     local roll=rnd()
     if roll>0.66 then
      make_tree(x,y)
     elseif roll>0.33 then
      make_pond(x,y)
     else
      make_field(x,y)
     end
    end
   end
  end
  sort_entities()
 end
end

function show_dmg_hilight(e)
 e.hilight=ondmg_hilight
end

function play_sound(n)
 sfx(n)
end

function get_icon(r)
 local icon=nil
 for kvp in all(slot_spr) do
  if kvp[1]==r then
   icon=kvp[2]
   break
  end
 end
 return icon
end

function show_preview_vfx()
	clear_preview_vfx()
 local card=cards[card_i]
 if card:can_afford() then
  cache_terrain_pos()
  for p in all(card.pattern) do
   local x=player.gx+p[1]
   local y=player.gy+p[2]
   if card.card_type=="attack"
    or card.card_type==""
    or card.card_type=="grow"
     and x>0 and x<=grid.w
     and terrain_pos[x][y]==nil
    then
    add(grid.hilights,{x,y})
   end
  end
  if card.card_type=="attack" then
	  marked_enemies={}
	  for e in all(enemies) do
	   for th in all(grid.hilights) do
	    if e.gx==th[1] and e.gy==th[2] then
	     e.hilight=dmg_hilight
	     add(marked_enemies,e)
	     break
	    end
	   end
	  end
	 end
  
  local earth_needed=card.earth
  local water_needed=card.water
  local fire_needed=card.fire
  marked_terrain={}
  for e in all(adj_terrain) do
   if e.element=="earth" and earth_needed>0 then
   	add(marked_terrain,e)
   	e.hilight=red_hilight
   	earth_needed-=1
   elseif e.element=="water" and water_needed>0 then
   	add(marked_terrain,e)
   	e.hilight=red_hilight
   	water_needed-=1
   elseif e.element=="fire" and fire_needed>0 then
   	add(marked_terrain,e)
   	e.hilight=red_hilight
   	fire_needed-=1
   end
  end
 end
end

function clear_preview_vfx()
 for e in all(enemies) do
  e.hilight=nil
 end 
 for e in all(terrain) do
  e.hilight=nil
 end
 grid.hilights={}
end

function _draw()
 if control_state=="title" then
	 --bg
	 rectfill(0,0,128,128,5)
	 foreach(title_entities,do_update)
	 foreach(title_entities,do_draw)
	 
	 circfill(38,58,16,0)
	 rectfill(50,55,82,63,0)
	 spr(128,20,40,4,4)
	 print("eomancy",54,57,7)
	 
	 if t%45<30 then
		 local m="press ❎"
		 rectfill(64-#m*2-2,75,64+#m*3-2,83,0)
		 print(m,64-#m*2,77,7)
	 end
	 
	 rectfill(84,119,127,127,1)
	 print("ted carter",86,121,6)
	 
	 return 
 end

 --bg
 rectfill(0,0,128,128,5)

 --grid
 grid:draw()
 if control_state=="tile_select" and t%30<20 then
  local bounds=grid:get_bounds(my_cursor.x,my_cursor.y)
  if cursor_valid then
   rectfill(bounds[1]+1,bounds[2]+1,bounds[3]-1,bounds[4]-1,10)
  else
   rect(bounds[1]+1,bounds[2]+1,bounds[3]-1,bounds[4]-1,8)
  end
 end
 
 foreach(entities,do_draw)
 foreach(particles,do_draw)

 --ui
 rectfill(0,83,128,128,1)
 if control_state=="enemy_turn" then
  print("enemy turn",2,85,6)
 else
	 local act_string="actions:"
	 for i=1,my_actions do
	  act_string=act_string.."⧗"
	 end
	 print(act_string,2,85,7)
 end
 
 print("wave:"..wave_i.."/"..#waves,95,85,7)
 
 --cards
 local y=6*16
 for i=1,#cards do
  if control_state!="card_select" or i!=card_i then
   cards[i]:draw(card_x,y,false)
  end
 end
 if control_state=="card_select" then
	 local t_diff=abs(t-card_t)
	 if t_diff<10 then
	  y-=t_diff/3
	 else
	  y=y-3.5+sin((t_diff-15)*0.02)*1.2
	 end 
	 cards[card_i]:draw(card_x,y,true)
 elseif control_state=="rolling" then
  --draw slots
  -- slot_spd is in slots/sec
  local target=marked_terrain[1]
  local left=min(target.x+6,115)
  local top=max(target.y-28,0)
  rectfill(left,top,left+8,top+24,13)
  rectfill(left,top+2,left+8,top+8*3-2,6)
  rectfill(left,top+6,left+8,top+8*3-6,7)
  
  local pos=t/30*slot_spd
  local result_i=flr(pos)%#target.pool
  for i=-2,2 do
   local r=target.pool[(result_i+i)%#target.pool+1]
   local icon=get_icon(r)
	  spr(icon,left,top+4+8*(pos-flr(pos))-8*i)
  end  

  rectfill(left,top-12,left+8,top,9)
  rectfill(left,top+24,left+8,top+36,9)
  rect(left,top-8,left+8,top+32,9)
  spr(11,left,top+8)
 end
 
 --toasts
 for toast in all(toasts) do
  if toast.bg!=nil then
   rectfill(toast.x-1,toast.y-1,toast.x+toast.w*8,toast.y+toast.h*8,toast.bg)
  end
  spr(toast.sprite,toast.x,toast.y,toast.w,toast.h)
 end
 
 --flybys
 for f in all(flybys) do
  local m_len=4*#f.message
  local left=f.x-4
  local top=f.y-3
  local right=f.x+m_len+1
  local bot=f.y+9
  
  rectfill(left,top,right,bot,0)
  rect(left,top,right,bot,7)
  print(f.message,f.x,f.y,7)

  f.t+=1
  local p_t=f.t/f.life
  if p_t<0.3 then
   f.x+=(f.x_target-f.x)*0.2
  elseif p_t>0.6 then
   if p_t>=1 then
    del(flybys,f)
   else
    f.x+=((-4-m_len)-f.x)*0.2
   end
  end
 end
 
 if win!=nil then
  local m="oh no! try again!"
  local c=8
  if win then
   m="you win!"
   c=10
  end
  rectfill(20,55,108,75,0)
  rect(20,55,108,75,7)
  print(m,64-2*#m,62,c)
 end
 
 --debug
 if is_debug then
	 for i=1,#debug do
	  print(debug[i],2,2+7*i,8)
	 end
 end
 if should_clear then
  debug={}
 end
end

function quick_sort_by_y(array,p,r)
 if 0<r-p then
  local q=partition_by_y(array,p,r)
  quick_sort_by_y(array,p,q-1)
  quick_sort_by_y(array,q+1,r)
 end
end

function partition_by_y(array,p,r)
 local q=p
 for i=p,r-1 do
  if array[i].y<=array[r].y then
   swap(array,i,q)
   q+=1
  end
 end
 swap(array,r,q)
 return q
end

function swap(array,first,second)
 local temp=array[first]
 array[first]=array[second]
 array[second]=temp
end
__gfx__
000000000009800000000000660660660000000000000000000bb00bb00000004900400400000033000000000000000000000000000000000000000100000000
00000000000998000009800000000000000000000000000000bbbbb33bbb30000404a0490003b3b3005555000000000000000000000000000000000110000000
000000000099998000099800000000000000000000000000000b3bb00b3330000049a400003b3bb3057777500000000000000001111000000000000111000000
00000000077977980099998000000000000000000000000000003b33443300004aaaa94003b3bbb0577777754000000000000110000110000000000111100000
00000000097979980779779800000000000000000000000000bb33bb40bb3000049aaaa43b3bbb30578778754400000000000101000010000000000111110000
00000000099aa998097a799800000000000000666660000000b33bb33bb33000004a94003bbbb300057777504000000000001000000001000111111111111000
0000000009aaaa9009aaaa9000000000000006556556000000004444b3300000940a40400b333000005775000000000000001000000001000111111111111100
00000000009aa900009aa900000000000000666565556000000000444b3000004004009403000000000550000000000000001000000001000111111111111110
0000000000000000606606600660660600066666665556000000b004400000000000000000000000000000000000000000001000000001000111111111111110
00000000000000000000000000000000000666666665560000000444400000000001111000000000000000000000000000000100000010000111111111111100
00000000000000000000000000000000000666656666560000000004400000000041444000011110000111100001111000001110000110000111111111111000
000000000000000000000000000000000006666656665600000000044000000000144b4000414440004144400001444000011101111000000000000111110000
00000bb0000000000000000000000000000066666666600000000004400000000014444000144b4000144b400044444000111000000000000000000111100000
000bb0bb0bbb000000000000000000000000066665566000000000444000000000044440001444400014444000144b4001110000000000000000000111000000
0000bbb0bb00000000000000000000000000006666600000000004444440000000a3430000044440000444400004444001100000000000000000000110000000
000000bb00000000000000000000000000000000000000000000000000000000033adda000a3430000a3430000a3430000000000000000000000000100000000
00000004000000000000000000000000000000000000000000000000000000000333dd30033adda0033adda0033adda000000000000000000700070001098010
000000b4000000000000000000000000000000000000000000000000000000000034dd400333dd300333dd300333dd3000000000070007000777700090199109
0000000400000000000000000700070000000000000000000000000000000000000ddd000333dd300043dd340043dd3400000000077770000fff7f0009999980
0000000440000000070007000777700000000000000000000000000000000000000ddd00004ddd04000ddd00000ddd00000000070fff7f000f8ff70001191198
0000000440000000077770000fff7f000000000000000000000000000000000000dd0dd000ddddd000ddddd000ddddd0000000070f8ff7000fff570099191998
00000004400000000fff7f000f8ff7000000000000000000000000000000000000dd0dd000dd0dd000dd0dd000dd0dd0000000770ffff5000fff500019999918
00000044440000000f8ff7000ffff7000000000000000000000000000000000000110110011000110110001101100011000000770ffff500006f520091717190
00000000000000000ffff7000ffff000000000000000000000000000000000000000000000000000000000000000000000000777006f65000266566009111900
00000000000000000ffff000006f660000000000000000000000000b000000000000000000000000000000000000000000000777026625660666566600000000
0000000000000000006f660005775770000000000000000000000004000000000144440a3400dd10000000000000000000000077066665660f66ff6001098010
0000000000000000577775700f77ff000000000000000000000000b4000000000140443ddddddd10000000000000000000000077f6666ff00022520090199109
0000000000000000f0777ff005065000000000000000000000000004000000000144444ddddd0000000000000000000000000007002225000066560009999980
00000000000000005006650005775700000000b00000000000000004000000000114443a34dddd10000000000000000000000007066665600660566001191198
000000000000000057777570077077000000000b000000000000000400000000004110a33300dd10000000000000000000000000066065600660566099191998
000000000000000077000770077077000000000b0000000000000004400000000000000330000000000000000000000000000000660005660660666099999990
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000009717900
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000b000000b00000000a000000a0000
00000000000000000000000000000000000000000000000000ee03000000ee0000000000000000000000000000000000000b00000000b0b0000f00000000f0a0
000000000000000000000000000000000000000000000b0000ee00300030ee000000000000000000000b00000000b000b00f0b00bb00f00ba00f0a00af00f00f
00000000000000000000000000000000000000000000b00000030030003030000000000000000000000b0b000b00b00b0b0f0f0b0fb0f0bf0f0f0f0a0fa0f0ff
00000000000000000000000000000000000b00000000b0000003000000003030000b00000000b0000b0f0b0b0b00f00b0f0fbf0b0f0bf0bf0f0faf0f0f0ff0ff
00000000000000000000000000000000000b011111111000000301111111103000000b000b00000b0b00bf0b0f0b00bf0f00bf0f0f0b00ff0f00ff0f0f0f00ff
0000000000000000000001111100000000011ccccccc110000111ccccccc11100b00000b000000000f00b00b000b00b00f00f00f000f00f00f00f00f000f00f0
000000cc11110000000011c7cc110000001c7cc777ccc10001cc7cc777ccccc10000b00b000b00b00000b00f000b00f00000f00f000f00f00000f00f000f00f0
000000011000000000000cccccc000000000cccccccccc0000ccccccccccccc00000b000000b00000000f000000f00000000f000000f00000000f000000f0000
00000000000000000000000000000000000000ccc0000000000ccccccccccc000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000cccccc000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000077777777000000000000000008888880000000000000000000000000000000000000000000000000000000000000000000000000000000000000
000000000777cccccccc777000000000000008888880000000000000000000000000000000000000000000000000000000000000000000000000000000000000
000000077cccccccccccccc770000000000008888880000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000007ccccccccccbbbccccc7000000008888888888880000000000000000000000000000000000000000000000000000000000000000000000000000000000
000077ccccbbbbbbbbbbbccccc770000008888888888880000000000000000000000000000000000000000000000000000000000000000000000000000000000
00007cccccbbbbbbbbbbbbccccc70000008888888888880000000000000000000000000000000000000000000000000000000000000000000000000000000000
0007ccccccbbb7777777bbcccccc7000008888888888880000000000000000000000000000000000000000000000000000000000000000000000000000000000
007cccccccc77000000077ccccccc700008888888888880000000000000000000000000000000000000000000000000000000000000000000000000000000000
07cccccccc7000000000007ccccccc70008888888888880000000000000000000000000000000000000000000000000000000000000000000000000000000000
07ccccccc700000000000007cccccc70000008888880000000000000000000000000000000000000000000000000000000000000000000000000000000000000
07bbcccc700000000000000077777770000008888880000000000000000000000000000000000000000000000000000000000000000000000000000000000000
7bbbbbcc700000000000000000000000000008888880000000000000000000000000000000000000000000000000000000000000000000000000000000000000
7bbbbbc7000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
7bbbbbc7000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
7bbbbbc7000000000077777777777777000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
7bbbbbc700000000007ccccbbbbbbbb7000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
7bbbbbc700000000007ccccbbbbbbbb7000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
7bbbbcc700000000007cccccbbbbbbb7000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
7bbbbbcc70000000007ccccccbbbbbb7000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
07bbbbcc70000000007cccccccbbbb70000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
07bbbbbcc70000000077777cccbbbb70000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
07bbbbbccc7000000000007cccbbbb70000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
007bbbbcccc77000000077ccccbbb700000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0007bbccccccc7777777cccccccb7000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00007cccccccccbbbbccccccccc70000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
000077cccccccccbbbcccccccc770000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000007cccccccccbbccccccc7000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
000000077ccccccccbccccc770000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
000000000777cccccccc777000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000077777777000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
__label__
55555555555555555555555555555555555bb55bb555555555555555555555555555555555555555555555555555555555555555555555555555555555555555
5555555555555555555555555555555555bbbbb33bbb355555555555555555555555555555555555555555555555555555555555555555555555555555555555
55555555555555555555555555555555555b3bb55b33355555555555555555555555555555555555555555555555555555555555555555555555555555555555
5555555555555555555555555555555555553b334433555555555555555555555555555555555555555555555555555555555555555555555555555555555555
5555555555555555555555555555555555bb33bb45bb355555555555555555555555555555555555555555555555555555555555555555555555555555555555
5555a555555a5555555555555555555555b33bb33bb3355555555555555555555555a555555a55555555a555555a55555555a555555a55555555555555555555
555f55555555f5a555ee53555555ee5555554444b33555555555555555555555555f55555555f5a5555f55555555f5a5555f55555555f5a555ee53555555ee55
a55f5a55af55f55f55ee55355535ee55555555444b3555555555555555555555a55f5a55af55f55fa55f5a55af55f55fa55f5a55af55f55f55ee55355535ee55
5f5f5f5a5fa5f5ff55535535553535555555b5544555555555555555555555555f5f5f5a5fa5f5ff5f5f5f5a5fa5f5ff5f5f5f5a5fa5f5ff5553553555353555
5f5faf5f5f5ff5ff5553555555553535555554444555555555555555555555555f5faf5f5f5ff5ff5f5faf5f5f5ff5ff5f5faf5f5f5ff5ff5553555555553535
5f55ff5f5f5f55ff5553511111111535555555544555555555555555555555555f55ff5f5f5f55ff5f55ff5f5f5f55ff5f55ff5f5f5f55ff5553511111111535
5f55f55f555f55f555111ccccccc1115555555544555555555555555555555555f55f55f555f55f55f55f55f555f55f55f55f55f555f55f555111ccccccc1115
5555f55f555f55f551cc7cc777ccccc1555555544555555555555555555555555555f55f555f55f55555f55f555f55f55555f55f555f55f551cc7cc777ccccc1
5555f555555f555555ccccccccccccc5555555444555555555555555555555555555f555555f55555555f555555f55555555f555555f555555ccccccccccccc5
5555555555555555555ccccccccccc5555555444444555555555555555555555555555555555555555555555555555555555555555555555555ccccccccccc55
555555555555555555555cccccc555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555cccccc55555
5555555555555555555bb55bb5555555555555555555555555555555555555555555555555555555555bb55bb5555555555bb55bb55555555555555555555555
555555555555555555bbbbb33bbb355555555555555555555555555555555555555555555555555555bbbbb33bbb355555bbbbb33bbb35555555555555555555
5555555555555555555b3bb55b333555555555555555555555555555555555555555555555555555555b3bb55b333555555b3bb55b3335555555555555555555
555555555555555555553b334433555555555555555555555555555555555555555555555555555555553b334433555555553b33443355555555555555555555
555555555555555555bb33bb45bb355555555555555555555555555555555555555555555555555555bb33bb45bb355555bb33bb45bb35555555555555555555
555555555555555555b33bb33bb335555555a555555a55555555555555555555555555555555555555b33bb33bb3355555b33bb33bb335555555555555555555
55ee53555555ee5555554444b3355555555f55555555f5a55555555555555555555555555555555555554444b335555555554444b335555555ee53555555ee55
55ee55355535ee55555555444b355555a55f5a55af55f55f55555555555555555555555555555555555555444b355555555555444b35555555ee55355535ee55
55535535553535555555b554455555555f5f5f5a5fa5f5ff555555555555555555555555555555555555b554455555555555b554455555555553553555353555
555355555555353555555444455555555f5faf5f5f5ff5ff55555555555555555555555555555555555554444555555555555444455555555553555555553535
555351111111153555555554455555555f55ff5f5f5f55ff55555555555555555555555555555555555555544555555555555554455555555553511111111535
55111ccccccc111555555554455555555f55f55f555f55f5555555555555555555555555555555555555555445555555555555544555555555111ccccccc1115
51cc7cc777ccccc155555554455555555555f55f555f55f5555555555555555555555555555555555555555445555555555555544555555551cc7cc777ccccc1
55ccccccccccccc555555544455555555555f555555f5555555555555555555555555555555555555555554445555555555555444555555555ccccccccccccc5
555ccccccccccc55555554444445555555555555555555555555555555555555555555555555555555555444444555555555544444455555555ccccccccccc55
55555cccccc5555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555cccccc55555
55555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555
55555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555
55555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555
55555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555
55555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555
5555a555555a55555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555
555f55555555f5a5555555555555555555ee53555555ee55555555555555555555ee53555555ee55555555555555555555ee53555555ee5555ee53555555ee55
a55f5a55af55f55f555555555555555555ee55355535ee55555555555555555555ee55355535ee55555555555555555555ee55355535ee5555ee55355535ee55
5f5f5f5a5fa5f5ff5555555555555555555355355535355555555555555555555553553555353555555555555555555555535535553535555553553555353555
5f5faf5f5f5ff5ff5555555555555555555355555555353555555555555555555553555555553535555555555555555555535555555535355553555555553535
5f55ff5f5f5f55ff5555555555555555777777770011153555555555555555555553511111111535555555555555555555535111111115355553511111111535
5f55f55f555f55f55555555555555777cccccccc77700115555555555555555555111ccccccc1115555555555555555555111ccccccc111555111ccccccc1115
5555f55f555f55f55555555555577cccccccccccccc77001555555555555555551cc7cc777ccccc1555555555555555551cc7cc777ccccc151cc7cc777ccccc1
5555f555555f555555555555557ccccccccccbbbccccc700555555555555555555ccccccccccccc5555555555555555555ccccccccccccc555ccccccccccccc5
55555555555555555555555577ccccbbbbbbbbbbbccccc770055555555555555555ccccccccccc555555555555555555555ccccccccccc55555ccccccccccc55
5555555555555555555555557cccccbbbbbbbbbbbbccccc7000555555555555555555cccccc55555555555555555555555555cccccc5555555555cccccc55555
555555555555555555555557ccccccbbb7777777bbcccccc700bb55bb5555555555bb55bb5555555555555555555555555555555555555555555555555555555
55555555555555555555557cccccccc77000000077ccccccc700bbb33bbb355555bbbbb33bbb3555555555555555555555555555555555555555555555555555
5555555555555555555557cccccccc7000000000007ccccccc700bb55b333555555b3bb55b333555555555555555555555555555555555555555555555555555
5555555555555555555557ccccccc700000000000007cccccc700b334433555555553b3344335555555555555555555555555555555555555555555555555555
5555555555555555555557bbcccc70000000000000007777777000bb45bb355555bb33bb45bb3555555555555555555555555555555555555555555555555555
555555555555555555557bbbbbcc70000000000000000000000000b33bb3355555b33bb33bb335555555a555555a555555555555555555555555555555555555
555555555555555555557bbbbbc70000000000000000000000000044b335555555554444b3355555555f55555555f5a555555555555555555555555555555555
555555555555555555557bbbbbc70000000000000000000000000000000000000000000000000000000f5a55af55f55f555b55555555b5555555555555555555
555555555555555555557bbbbbc70000000000777777777777770000000000000000000000000000000f5f5a5fa5f5ff555b5b555b55b55b5555555555555555
555555555555555555557bbbbbc700000000007ccccbbbbbbbb70077700770777077707700077070700faf5f5f5ff5ff5b5f5b5b5b55f55b5555555555555555
555555555555555555557bbbbbc700000000007ccccbbbbbbbb700700070707770707070707000707005ff5f5f5f55ff5b55bf5b5f5b55bf5555555555555555
555555555555555555557bbbbcc700000000007cccccbbbbbbb700770070707070777070707000777005f55f555f55f55f55b55b555b55b55555555555555555
555555555555555555557bbbbbcc70000000007ccccccbbbbbb700700070707070707070707000007005f55f555f55f55555b55f555b55f55555555555555555
5555555555555555555557bbbbcc70000000007cccccccbbbb7000777077007070707070700770777005f555555f55555555f555555f55555555555555555555
5555555555555555555557bbbbbcc70000000077777cccbbbb700000000000000000000000000000000555555555555555555555555555555555555555555555
5555555555555555555557bbbbbccc7000000000007cccbbbb700000000000000000000000000000000555555555555555555555555555555555555555555555
555bb55bb55555555555557bbbbcccc77000000077ccccbbb700005bb55555555555555555555555555bb55bb55555555555555555555555555bb55bb5555555
55bbbbb33bbb355555555557bbccccccc7777777cccccccb70000bb33bbb3555555555555555555555bbbbb33bbb3555555555555555555555bbbbb33bbb3555
555b3bb55b333555555555557cccccccccbbbbccccccccc700000bb55b3335555555555555555555555b3bb55b3335555555555555555555555b3bb55b333555
55553b33443355555555555577cccccccccbbbcccccccc7700003b3344335555555555555555555555553b3344335555555555555555555555553b3344335555
55bb33bb45bb355555555555557cccccccccbbccccccc700000b33bb45bb3555555555555555555555bb33bb45bb3555555555555555555555bb33bb45bb3555
55b33bb33bb335555555555555077ccccccccbccccc7700000033bb33bb33555555555555555555555b33bb33bb33555555555555555555555b33bb33bb33555
55554444b33555555555555555500777cccccccc7770000000554444b3355555555555555555555555554444b3355555555555555555555555554444b3355555
555555444b35555555555555555550007777777700000000555555444b3555555555555555555555555555444b3555555555555555555555555555444b355555
5555b55445555555555555555555550000000000000000055555b5544555555555555555555555555555b5544555555555555555555555555555b55445555555
55555444455555555555555555555555000000000000055555555444455555555555555555555555555554444555555555555555555555555555544445555555
55555554455555555555555555555555555000000055555555555554455555555555555555555555555555544555555555555555555555555555555445555555
55555554455555555555555555555555555555544555555555555554455555555555555555555555555555544555555555555555555555555555555445555555
55555554455555555555555555555555555555544555555555555554455555555555555555555555555555544555555555555555555555555555555445555555
55555544455555555555555555555555555555444555555555555544455555555555555555555555555555444555555555555555555555555555554445555555
55555444444555555555555555555555555554444445555555555444444555555555555555555555555554444445555555555555555555555555544444455555
55555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555
555bb55bb5555555555555555555555555555555555555555555555555555555555bb55bb55555555555555555555555555bb55bb5555555555bb55bb5555555
55bbbbb33bbb355555555555555555555555555555555555555555555555555555bbbbb33bbb3555555555555555555555bbbbb33bbb355555bbbbb33bbb3555
555b3bb55b333555555555555555555555555555555555555555555555555555555b3bb55b3335555555555555555555555b3bb55b333555555b3bb55b333555
55553b334433555555555555555555555555555555555555555555555555555555553b3344335555555555555555555555553b334433555555553b3344335555
55bb33bb45bb355555555555555555555555555555555555555555555555555555bb33bb45bb3555555555555555555555bb33bb45bb355555bb33bb45bb3555
55b33bb33bb3355555555555555555555555555555555555555555555555555555b33bb33bb335555555a555555a555555b33bb33bb3355555b33bb33bb33555
55554444b3355555555555555555555555ee53555555ee55555555555555555555554444b3355555555f55555555f5a555554444b335555555554444b3355555
555555444b355555555555555555555555ee55355535ee555555555555555555555555444b355555a55f5a55af55f55f555555444b355555555555444b355555
5555b554455555555555555555555555555355355535355555555555555555555555b554455555555f5f5f5a5fa5f5ff5555b554455555555555b55445555555
555554444555555555555555555555555553555555553535555555555555555555555444455555555f5faf5f5f5ff5ff55555444455555555555544445555555
555555544555555555555555555555555553511111111535555555555555555555555554455555555f55ff5f5f5f55ff55555554455555555555555445555555
5555555445555555555555555555555555111ccccccc1115555555555555555555555554455555555f55f55f555f55f555555554455555555555555445555555
5555555445555555555555555555555551cc7cc777ccccc1555555555555555555555554455555555555f55f555f55f555555554455555555555555445555555
5555554445555555555555555555555555ccccccccccccc5555555555555555555555544455555555555f555555f555555555544455555555555554445555555
55555444444555555555555555555555555ccccccccccc5555555555555555555555544444455555555555555555555555555444444555555555544444455555
5555555555555555555555555555555555555cccccc5555555555555555555555555555555555555555555555555555555555555555555555555555555555555
55555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555
55555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555
55555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555
55555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555
55555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555
5555a555555a55555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555
555f55555555f5a555ee53555555ee55555555555555555555ee53555555ee5555ee53555555ee55555555555555555555ee53555555ee555555555555555555
a55f5a55af55f55f55ee55355535ee55555555555555555555ee55355535ee5555ee55355535ee55555555555555555555ee55355535ee555555555555555555
5f5f5f5a5fa5f5ff5553553555353555555555555555555555535535553535555553553555353555555555555555555555535535553535555555555555555555
5f5faf5f5f5ff5ff5553555555553535555555555555555555535555555535355553555555553535555555555555555555535555555535355555555555555555
5f55ff5f5f5f55ff5553511111111535555555555555555555535111111115355553511111111535555555555555555555535111111115355555555555555555
5f55f55f555f55f555111ccccccc1115555555555555555555111ccccccc111555111ccccccc1115555555555555555555111ccccccc11155555555555555555
5555f55f555f55f551cc7cc777ccccc1555555555555555551cc7cc777ccccc151cc7cc777ccccc1555555555555555551cc7cc777ccccc15555555555555555
5555f555555f555555ccccccccccccc5555555555555555555ccccccccccccc555ccccccccccccc5555555555555555555ccccccccccccc55555555555555555
5555555555555555555ccccccccccc555555555555555555555ccccccccccc55555ccccccccccc555555555555555555555ccccccccccc555555555555555555
555555555555555555555cccccc55555555555555555555555555cccccc5555555555cccccc55555555555555555555555555cccccc555555555555555555555
5555555555555555555555555555555555555555555555555555555555555555555bb55bb5555555555555555555555555555555555555555555555555555555
555555555555555555555555555555555555555555555555555555555555555555bbbbb33bbb3555555555555555555555555555555555555555555555555555
5555555555555555555555555555555555555555555555555555555555555555555b3bb55b333555555555555555555555555555555555555555555555555555
555555555555555555555555555555555555555555555555555555555555555555553b3344335555555555555555555555555555555555555555555555555555
555555555555555555555555555555555555555555555555555555555555555555bb33bb45bb3555555555555555555555555555555555555555555555555555
555555555555555555555555555555555555555555555555555555555555555555b33bb33bb335555555a555555a555555555555555555555555555555555555
55ee53555555ee5555ee53555555ee5555ee53555555ee55555555555555555555554444b3355555555f55555555f5a555ee53555555ee555555555555555555
55ee55355535ee5555ee55355535ee5555ee55355535ee555555555555555555555555444b355555a55f11111111111111111111111111111111111111111111
55535535553535555553553555353555555355355535355555555555555555555555b554455555555f5f11111111111111111111111111111111111111111111
555355555555353555535555555535355553555555553535555555555555555555555444455555555f5f11666166616611111116616661666166616661666111
555351111111153555535111111115355553511111111535555555555555555555555554455555555f5511161161116161111161116161616116116111616111
55111ccccccc111555111ccccccc111555111ccccccc1115555555555555555555555554455555555f5511161166116161111161116661661116116611661111
51cc7cc777ccccc151cc7cc777ccccc151cc7cc777ccccc155555555555555555555555445555555555511161161116161111161116161616116116111616111
55ccccccccccccc555ccccccccccccc555ccccccccccccc555555555555555555555554445555555555511161166616661111116616161616116116661616111
555ccccccccccc55555ccccccccccc55555ccccccccccc5555555555555555555555544444455555555511111111111111111111111111111111111111111111
55555cccccc5555555555cccccc5555555555cccccc5555555555555555555555555555555555555555511111111111111111111111111111111111111111111

__map__
0313120313120313120313120313120313120000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
__sfx__
0101000d2002000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0110000028735297352b7350000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
011000002b755287552b7552f75500000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0110000015454154511c4511c45500000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
010900001163310625000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
010900001f64311625000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
011200002364313625000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
011200002b64310625000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
011100002f6531f625300002400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
011000003b6532b625000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
011800002154221532215222151200000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
013800001755300000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
011000002d71100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
011000002d72300000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
011000001a5471d537215272351700000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
